﻿using CronParse;

namespace ConsoleApp1
{
    internal class Program
    {
        static void Main(string[] args)
        {
            var listCron = new List<CronStruct>()
            {
                new CronStruct("0 0 12 ? * WED"),
                new CronStruct("0 0/5 14,18 * * ?"),
                new CronStruct("0 15 10 10-20/3 * ?"),
                new CronStruct("0 15 10 ? * 6L 2024-2030"),
                new CronStruct("15,30,45 * * * * ?"),
                new CronStruct("15-30/5 * * * * ?"),
                new CronStruct("5-10 * * * FEB ?")
            };
            DateTime dt = DateTime.Now;
            Console.WriteLine(dt.ToString("yyyy-MM-dd HH:mm:ss"));
            foreach (CronStruct expr in listCron)
            {
                Console.WriteLine("Single：{0}", expr.GetNextTime(dt, true).ToString("yyyy-MM-dd HH:mm:ss"));
                var msArr = expr.GetNextMilliseconds(dt, out var times);
                foreach (var item in times)
                {
                    Console.WriteLine("{0}\t\t{1}", expr, item.ToString("yyyy-MM-dd HH:mm:ss"));
                }
                foreach (var item in msArr)
                {
                    Console.WriteLine("{0}\t\t{1}\t\t{2}", expr, item, dt.AddMilliseconds(item).ToString("yyyy-MM-dd HH:mm:ss"));
                }
            }

            Console.WriteLine("Hello, World!");
            Console.ReadLine();
        }
    }
}
