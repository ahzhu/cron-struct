﻿using System.Globalization;
using System.Text.RegularExpressions;

namespace CronParse
{
    /// <summary>
    /// Cron 表达式
    /// </summary>
    public readonly struct CronStruct : IComparable, IComparable<CronStruct>, IEquatable<CronStruct>
    {
        private readonly static Regex RegNumber = new Regex(@"^[0-9]+$", RegexOptions.Compiled);
        private readonly static string[] MonthNames = new string[] { "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC" };
        private readonly static string[] WeekNames = new string[] { "SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT" };
        private readonly static int[] _empty = new int[0];
        private readonly static int[] _minuteAndSecond;
        private readonly static int[] _hourArr;
        private readonly static int[] _monthArr;
        private const char Comma = ',';
        private const char Hyphen = '-';
        private const string Asterisk = "*";
        private const string Question = "?";
        private const char Slash = '/';
        private const char L = 'L';
        private const char C = 'C';
        private const char W = 'W';
        private const string LW = "LW";
        private const char Hash = '#';
        private const char SplitChar = ' ';
        private const string DefaultExpression = "* * * * * ?";

        private const int MaxHour = 23;
        private const int MaxSecondMinute = 59;
        private const int TickSecond = 1000;
        private const int TickMinute = TickSecond * 60;
        private const int TickHour = TickMinute * 60;
        private const int TickDay = TickHour * 24;

        private readonly string _expr;
        private readonly string _secondExp = string.Empty;
        private readonly string _minuteExp = string.Empty;
        private readonly string _hourExp = string.Empty;
        private readonly string _dayExp = string.Empty;
        private readonly string _weekExp = string.Empty;
        private readonly string _monthExp = string.Empty;
        private readonly string _yearExp = string.Empty;

        private readonly static Calendar _calendar;

        static int CurrentYear { get; set; }
        static int CurrentMonth { get; set; }
        static int CurrentDay { get; set; }

        public CronStruct() : this(DefaultExpression) { }

        static CronStruct()
        {
            var calendar = (CultureInfo.CurrentCulture ?? CultureInfo.DefaultThreadCurrentCulture)?.Calendar;
            if (calendar == null)
            {
                calendar = new GregorianCalendar();
            }
            _calendar = calendar;
            _minuteAndSecond = Enumerable.Range(0, 60).ToArray();
            _hourArr = Enumerable.Range(0, 24).ToArray();
            _monthArr = Enumerable.Range(1, 12).ToArray();
        }

        public CronStruct(string cron)
        {
            _expr = (cron ?? DefaultExpression).Trim().ToUpper();
            string[] parts = _expr.Split(SplitChar);
            if (parts.Length < 6)
            {
                _expr = DefaultExpression;
                return;
            }
            for (int i = parts.Length - 1; i >= 0; i--)
            {
                var bit = (CronBit)i;
                var str = parts[i];

                switch (bit)
                {
                    case CronBit.Second:
                        _secondExp = str;
                        break;

                    case CronBit.Minute:
                        _minuteExp = str;
                        break;

                    case CronBit.Hour:
                        _hourExp = str;
                        break;

                    case CronBit.Day:
                        _dayExp = str;
                        break;

                    case CronBit.Month:
                        _monthExp = str;
                        break;

                    case CronBit.Week:
                        _weekExp = str;
                        break;

                    case CronBit.Year:
                        _yearExp = str;
                        break;
                }
            }
        }

        public DateTime GetNextTime(DateTime now)
        {
            DateTime curDate = now;
            int[] _years = GenerateYear(_yearExp);
            int[] _months = GenerateMonth(_monthExp);

            int[] _hours = GenerateHour(_hourExp);
            int[] _minutes = GenerateSecondOrMinute(_minuteExp);
            int[] _seconds = GenerateSecondOrMinute(_secondExp);
            if (_seconds.Length == 60)
            {
                return now.AddSeconds(1);
            }
            int curSecond = 0, curMinute = 0, curHour = 0, curDay = 0, curMonth = now.Month, curYear = now.Year;
            for (int i = 0; i < _seconds.Length; i++)
            {
                curSecond = _seconds[i];
                if (curSecond > now.Second)
                {
                    break;
                }
            }
            if (_minutes.Length == 60)
            {
                var newDt = new DateTime(now.Year, now.Month, now.Day, now.Hour, now.Minute, curSecond > now.Second ? curSecond : _seconds[0]);
                return curSecond > now.Second ? newDt : newDt.AddMinutes(1);
            }
            for (int i = 0; i < _minutes.Length; i++)
            {
                curMinute = _minutes[i];
                if (curMinute > now.Minute)
                {
                    break;
                }
            }
            if (_hours.Length == 24)
            {
                if (curMinute > now.Minute)
                {
                    return new DateTime(now.Year, now.Month, now.Day, now.Hour, curMinute, _seconds[0]);
                }
                if (curSecond > now.Second)
                {
                    return new DateTime(now.Year, now.Month, now.Day, now.Hour, curMinute, curSecond);
                }
                return new DateTime(now.Year, now.Month, now.Day, now.Hour, _minutes[0], _seconds[0]).AddHours(1);
            }
            for (int i = 0; i < _hours.Length; i++)
            {
                curHour = _hours[i];
                if (curHour > now.Hour)
                {
                    curSecond = _seconds[0];
                    curMinute = _minutes[0];
                    break;
                }
                if (curHour == now.Hour && (curSecond > now.Second || curMinute > now.Minute))
                {
                    break;
                }
            }
        RebuildDay:
            SetCurrentDate(curDate);
            int[] _weeks = GenerateWeek(_weekExp, out bool isLast01, out int itvDay1);
            int[] _days = GenerateDay(_dayExp, out bool isLast02, out int itvDay2);
            bool addMonth = false;
            bool addDay = false;
            if (_weeks.Length > 0)
            {
                for (int i = 0; i < _weeks.Length; i++)
                {
                    curDay = _weeks[i];
                    if (isLast01)
                    {
                        if (curDate <= now && curDay <= now.Day)
                        {
                            curDate = curDate.AddMonths(1);
                            curDate = new DateTime(curDate.Year, curDate.Month, 1, curDate.Hour, curDate.Minute, curDate.Second);
                            curYear = curDate.Year;
                            curMonth = curDate.Month;
                            goto RebuildDay;
                        }
                    }
                    else
                    {
                        if (curDay == now.Day)
                        {
                            curDate = curDate.AddDays(itvDay1);
                            curYear = curDate.Year;
                            curMonth = curDate.Month;
                            goto RebuildDay;
                        }
                    }
                }
            }
            else
            {
                for (int i = 0; i < _days.Length; i++)
                {
                    curDay = _days[i];
                    if (isLast02)
                    {
                        if (curDate <= now && curDay <= now.Day)
                        {
                            curDate = curDate.AddMonths(1);
                            curDate = new DateTime(curDate.Year, curDate.Month, 1, curDate.Hour, curDate.Minute, curDate.Second);
                            curYear = curDate.Year;
                            curMonth = curDate.Month;
                            goto RebuildDay;
                        }
                        continue;
                    }
                    if (curDay < now.Day || (curDay == now.Day && curHour < now.Hour))
                    {
                        continue;
                    }
                    if (curDay > now.Day)
                    {
                        curSecond = _seconds[0];
                        curMinute = _minutes[0];
                        curHour = _hours[0];
                        break;
                    }
                    if (curSecond > now.Second || curMinute > now.Minute || curHour > now.Hour)
                    {
                        break;
                    }
                }
                if (!isLast02)
                {
                    addDay = (curSecond == 0 || curSecond <= now.Second) && (curMinute == 0 || curMinute <= now.Minute) && (curHour == 0 || curHour <= now.Hour) && (curDay == 1 || curDay <= now.Day);
                    int maxDay = _days.Max();
                    if (curDay <= now.Day && curDay == maxDay)
                    {
                        return new DateTime(curYear, curMonth, _days.Min(), curHour, curMinute, curSecond).AddMonths(1);
                    }
                }
            }
            if (_months.Length == 12)
            {
                if (addDay)
                {
                    return new DateTime(curYear, curMonth, curDay, curHour, curMinute, curSecond).AddDays(itvDay2);
                }
                addMonth = (curSecond == 0 || curSecond <= now.Second) && (curMinute == 0 || curMinute <= now.Minute) && (curHour == 0 || curHour <= now.Hour) && (curDay == 1 || curDay <= now.Day) && curMonth <= now.Month;
            }
            else
            {
                for (int i = 0; i < _months.Length; i++)
                {
                    curMonth = _months[i];
                    if (curMonth >= now.Month)
                    {
                        break;
                    }
                }
            }
            if (addMonth)
            {
                return new DateTime(curYear, curMonth, curDay, curHour, curMinute, curSecond).AddMonths(1);
            }
            if (_years.Length == 0)
            {
                if ((curSecond == 0 || curSecond <= now.Second) && (curMinute == 0 || curMinute <= now.Minute) && (curHour == 0 || curHour <= now.Hour) && (curDay == 1 || curDay <= now.Day) && curMonth <= now.Month)
                {
                    return new DateTime(curYear, curMonth, curDay, curHour, curMinute, curSecond).AddYears(1);
                }
                return new DateTime(curYear, curMonth, curDay, curHour, curMinute, curSecond);
            }
            for (int i = 0; i < _years.Length; i++)
            {
                curYear = _years[i];
                if (curYear >= now.Year)
                {
                    break;
                }
            }
            return new DateTime(curYear, curMonth, curDay, curHour, curMinute, curSecond);
        }

        public IEnumerable<DateTime> GetNextTimes(DateTime now, int count = 10)
        {
            var list = new List<DateTime>();
            DateTime result = now;
            for (int i = 0; i < count; i++)
            {
                result = GetNextTime(result);
                list.Add(result);
            }
            return list;
        }

        public long[] GetNextMilliseconds(DateTime dt, int count = 10)
        {
            var times = GetNextTimes(dt, count);
            if (times?.Any() == true)
            {
                DateTime curDate = (new DateTime(dt.Year, dt.Month, dt.Day, dt.Hour, dt.Minute, dt.Second)).AddSeconds(1);
                var result = new List<long>();
                foreach (var item in times)
                {
                    if (item < curDate)
                    {
                        continue;
                    }
                    TimeSpan sub = item - curDate;
                    result.Add((long)sub.TotalMilliseconds);
                }
                return result.ToArray();
            }
            return new long[0];
        }

        public long[] GetNextMilliseconds(DateTime dt, out IEnumerable<DateTime> times, int count = 10)
        {
            times = GetNextTimes(dt, count);
            if (times?.Any() == true)
            {
                DateTime curDate = (new DateTime(dt.Year, dt.Month, dt.Day, dt.Hour, dt.Minute, dt.Second));
                var result = new List<long>();
                foreach (var item in times)
                {
                    if (item < curDate)
                    {
                        continue;
                    }
                    TimeSpan sub = item - curDate;
                    result.Add((long)sub.TotalMilliseconds);
                }
                return result.ToArray();
            }
            return new long[0];
        }

        static int GetValue(int[] data, int val)
        {
            if (data.Length == 0)
            {
                return val;
            }
            foreach (var item in data)
            {
                if (item >= val)
                {
                    return item;
                }
            }
            return data[0];
        }

        #region Generate Function

        static void SetCurrentDate(DateTime nowDT)
        {
            CurrentYear = nowDT.Year;
            CurrentMonth = nowDT.Month;
            int totalDays = _calendar.GetDaysInMonth(CurrentYear, CurrentMonth);
            if (nowDT.Day > totalDays)
            {
                DateTime dt = nowDT.AddMonths(1);
                DateTime nextDT = new DateTime(dt.Year, dt.Month, 1);
                CurrentYear = nextDT.Year;
                CurrentMonth = nextDT.Month;
                CurrentDay = nextDT.Day;
            }
            else
            {
                CurrentDay = nowDT.Day;
            }
        }

        static void SetCurrentDate(DateTime nowDT, int year, int month)
        {
            CurrentYear = year;
            CurrentMonth = month;
            int totalDays = _calendar.GetDaysInMonth(CurrentYear, CurrentMonth);
            if (nowDT.Day > totalDays)
            {
                DateTime dt = nowDT.AddMonths(1);
                DateTime nextDT = new DateTime(dt.Year, dt.Month, 1);
                CurrentYear = nextDT.Year;
                CurrentMonth = nextDT.Month;
                CurrentDay = nextDT.Day;
            }
            else
            {
                CurrentDay = nowDT.Day;
            }
        }

        static int[] GenerateSecondOrMinute(string str)
        {
            if (string.IsNullOrEmpty(str) || str.Equals(Asterisk) || str.Equals(Question))
            {
                return _minuteAndSecond;
            }
            string[] arr;
            if (str.Contains(Hyphen) && str.Contains(Slash))
            {
                int every = 0;
                arr = str.Split(Slash);
                int.TryParse(arr[1], out every);
                every = every < 0 || every > MaxSecondMinute ? 0 : every;
                if (every == 0)
                {
                    return _minuteAndSecond;
                }
                arr = arr[0].Split(Hyphen);
                return ConvertToIntRange(arr[0], arr[1], every,
                    () =>
                    {
                        return _minuteAndSecond;
                    }, MaxSecondMinute);
            }
            bool isSlash = str.IndexOf(Slash) > -1;
            if (isSlash)
            {
                arr = str.Split(Slash);
                int begin = 0, every = 0;
                int.TryParse(arr[0], out begin);
                int.TryParse(arr[1], out every);
                begin = begin < 0 || begin > MaxSecondMinute ? 0 : begin;
                every = every < 0 || every > MaxSecondMinute ? 0 : every;
                if (begin == 0 && every == 0)
                {
                    return _empty;
                }
                var list = new List<int>();
                for (int i = begin; i <= MaxSecondMinute; i += every)
                {
                    list.Add(i);
                }
                return list.ToArray();
            }
            bool isComma = str.IndexOf(Comma) > -1;
            if (isComma)
            {
                arr = str.Split(Comma);
                return ConvertToInt(arr, MaxSecondMinute);
            }
            bool isHyphen = str.IndexOf(Hyphen) > -1;
            if (isHyphen)
            {
                arr = str.Split(Hyphen);
                return ConvertToIntRange(arr[0], arr[1], 1, () =>
                {
                    return _empty;
                }, MaxSecondMinute);
            }
            if (int.TryParse(str, out var v))
            {
                return new int[] { v < 0 || v > MaxSecondMinute ? 0 : v };
            }
            return _minuteAndSecond;
        }

        static int[] GenerateHour(string str)
        {
            if (string.IsNullOrEmpty(str) || str.Equals(Asterisk) || str.Equals(Question))
            {
                return _hourArr;
            }
            string[] arr;
            if (str.Contains(Hyphen) && str.Contains(Slash))
            {
                int every = 0;
                arr = str.Split(Slash);
                int.TryParse(arr[1], out every);
                every = every < 0 || every > MaxHour ? 0 : every;
                if (every == 0)
                {
                    return _hourArr;
                }
                arr = arr[0].Split(Hyphen);
                return ConvertToIntRange(arr[0], arr[1], every,
                    () =>
                    {
                        return _hourArr;
                    }, MaxHour);
            }
            bool isComma = str.IndexOf(Comma) > -1;
            if (isComma)
            {
                arr = str.Split(Comma);
                return ConvertToInt(arr, MaxHour);
            }
            bool isHyphen = str.IndexOf(Hyphen) > -1;
            if (isHyphen)
            {
                arr = str.Split(Hyphen);
                return ConvertToIntRange(arr[0], arr[1], 1, () =>
                {
                    return _hourArr;
                }, MaxHour);
            }
            bool isSlash = str.IndexOf(Slash) > -1;
            if (isSlash)
            {
                arr = str.Split(Slash);
                int begin = 0, every = 0;
                int.TryParse(arr[0], out begin);
                int.TryParse(arr[1], out every);
                begin = begin < 0 || begin > MaxHour ? 0 : begin;
                every = every < 0 || every > MaxHour ? 0 : every;
                if (begin == 0 && every == 0)
                {
                    return _hourArr;
                }
                var list = new List<int>();
                for (int i = begin; i <= MaxHour; i += every)
                {
                    list.Add(i);
                }
                return list.ToArray();
            }
            if (int.TryParse(str, out var v))
            {
                return new int[] { v < 0 || v > MaxHour ? 0 : v };
            }
            return _hourArr;
        }

        static int[] GenerateDay(string str, out bool isLast, out int itv)
        {
            itv = 1;
            isLast = false;
            int totalDays = _calendar.GetDaysInMonth(CurrentYear, CurrentMonth);
            if (string.IsNullOrEmpty(str) || str.Equals(Asterisk) || str.Equals(Question))
            {
                return Enumerable.Range(CurrentDay, totalDays - CurrentDay + 1).ToArray();
            }
            string[] arr;
            if (str.Contains(Hyphen) && str.Contains(Slash))
            {
                int every = 0;
                arr = str.Split(Slash);
                int.TryParse(arr[1], out every);
                every = every < 0 || every > totalDays ? 0 : every;
                if (every == 0)
                {
                    return _empty;
                }
                arr = arr[0].Split(Hyphen);
                return ConvertToIntRange(arr[0], arr[1], every,
                    () =>
                    {
                        return _empty;
                    }, totalDays);
            }
            bool isComma = str.IndexOf(Comma) > -1;
            if (isComma)
            {
                arr = str.Split(Comma);
                return ConvertToInt(arr, totalDays, 1);
            }
            bool isHyphen = str.IndexOf(Hyphen) > -1;
            if (isHyphen)
            {
                arr = str.Split(Hyphen);
                return ConvertToIntRange(arr[0], arr[1], 1, () =>
                {
                    return Enumerable.Range(CurrentDay, totalDays - CurrentDay + 1).ToArray();
                }, totalDays, 1);
            }
            bool isSlash = str.IndexOf(Slash) > -1;
            if (isSlash)
            {
                arr = str.Split(Slash);
                int begin = 0, every = 0;
                int.TryParse(arr[0], out begin);
                int.TryParse(arr[1], out every);
                begin = begin < 1 || begin > totalDays ? 1 : begin;
                every = every < 1 || every > totalDays ? 1 : every;
                itv = every;
                if (begin == 1 && every == 1)
                {
                    return Enumerable.Range(CurrentDay, totalDays - CurrentDay + 1).ToArray();
                }
                var list = new List<int>();
                for (int i = begin; i <= totalDays; i += every)
                {
                    list.Add(i);
                }
                return list.ToArray();
            }
            if (int.TryParse(str, out var v))
            {
                return new int[] { v < 1 || v > totalDays ? 1 : totalDays };
            }
            if (str.Contains(LW))
            {
                isLast = true;
                return new int[] { GetLastWorkDayOfMonth(CurrentYear, CurrentMonth) };
            }
            if (str.Contains(L))
            {
                isLast = true;
                return new int[] { totalDays };
            }
            if (str.EndsWith(W) && int.TryParse(str.Replace(W.ToString(), string.Empty), out v))
            {
                isLast = true;
                v = v < 1 ? GetLastWorkDayOfMonth(CurrentYear, CurrentMonth) : GetNearWorkDayOfMonth(CurrentYear, CurrentMonth, v);
                return new int[] { v };
            }
            return Enumerable.Range(CurrentDay, totalDays - CurrentDay + 1).ToArray();
        }

        static int[] GenerateMonth(string str)
        {
            if (string.IsNullOrEmpty(str) || str.Equals(Asterisk) || str.Equals(Question))
            {
                return _monthArr;
            }
            string[] arr;
            int v;
            bool isComma = str.IndexOf(Comma) > -1;
            if (isComma)
            {
                arr = str.Split(Comma);
                return ConvertToInt(arr, MonthNames.Length, 1, (mth) =>
                {
                    v = Array.IndexOf(MonthNames, str);
                    return v < 1 ? 1 : (v + 1);
                });
            }
            bool isHyphen = str.IndexOf(Hyphen) > -1;
            if (isHyphen)
            {
                arr = str.Split(Hyphen);
                int bIdx = 0, eIdx = 0;
                if (!int.TryParse(arr[0], out bIdx))
                {
                    v = Array.IndexOf(MonthNames, arr[0]);
                    bIdx = v < 1 ? 1 : (v + 1);
                }
                if (!int.TryParse(arr[1], out eIdx))
                {
                    v = Array.IndexOf(MonthNames, arr[1]);
                    eIdx = v < 1 ? 1 : (v + 1);
                }
                return ConvertToIntRange(bIdx, eIdx, 1, () =>
                {
                    return _monthArr;
                });
            }
            bool isSlash = str.IndexOf(Slash) > -1;
            if (isSlash)
            {
                arr = str.Split(Slash);
                int begin = 0, every = 0;
                if (!int.TryParse(arr[0], out begin))
                {
                    v = Array.IndexOf(MonthNames, arr[0]);
                    begin = v < 1 ? 1 : (v + 1);
                }
                if (!int.TryParse(arr[1], out every))
                {
                    v = Array.IndexOf(MonthNames, arr[1]);
                    every = v < 1 ? 1 : (v + 1);
                }
                begin = begin < 1 || begin > MonthNames.Length ? 1 : begin;
                every = every < 1 || every > MonthNames.Length ? 1 : every;
                if (begin == 1 && every == 1)
                {
                    return _monthArr;
                }
                var list = new List<int>();
                for (int i = begin; i <= MonthNames.Length; i += every)
                {
                    list.Add(i);
                }
                return list.ToArray();
            }
            if (int.TryParse(str, out v))
            {
                return new int[] { v < 1 || v > MonthNames.Length ? 1 : v };
            }
            if (str.EndsWith(L))
            {
                return new int[] { 12 };
            }
            v = Array.IndexOf(MonthNames, str);
            return v < 0 ? _monthArr : new int[] { v + 1 };
        }

        static int[] GenerateWeek(string str, out bool isLast, out int itv)
        {
            itv = 7;
            isLast = false;
            if (string.IsNullOrEmpty(str) || str.Equals(Asterisk) || str.Equals(Question))
            {
                return _empty;
            }
            string[] arr;
            int v;
            bool isComma = str.IndexOf(Comma) > -1;
            if (isComma)
            {
                arr = str.Split(Comma);
                return ConvertToInt(arr, WeekNames.Length, 1, (mth) =>
                {
                    int v = Array.IndexOf(WeekNames, mth);
                    return GetAllMonthWeekDay(CurrentYear, CurrentMonth, CurrentDay, v < 1 ? 1 : (v + 1));
                });
            }
            bool isHyphen = str.IndexOf(Hyphen) > -1;
            if (isHyphen)
            {
                arr = str.Split(Hyphen);
                int bIdx = 0, eIdx = 0;
                if (!int.TryParse(arr[0], out bIdx))
                {
                    v = Array.IndexOf(WeekNames, arr[0]);
                    bIdx = v < 1 ? 1 : (v + 1);
                }
                if (!int.TryParse(arr[1], out eIdx))
                {
                    v = Array.IndexOf(WeekNames, arr[1]);
                    eIdx = v < 1 ? 1 : (v + 1);
                }
                int[] weekArr = ConvertToIntRange(bIdx, eIdx, 1, () =>
                {
                    return Enumerable.Range(1, WeekNames.Length).ToArray();
                });
                for (int i = 0; i < weekArr.Length; i++)
                {
                    v = weekArr[i];
                    weekArr[i] = GetNearDayWeek(CurrentYear, CurrentMonth, CurrentDay, v < 1 || v > WeekNames.Length ? 1 : v);
                }
                Array.Sort(weekArr);
                return weekArr.Distinct().ToArray();
            }
            bool isSlash = str.IndexOf(Slash) > -1;
            if (isSlash)
            {
                arr = str.Split(Slash);
                int begin = 0, every = 0;
                if (!int.TryParse(arr[0], out begin))
                {
                    v = Array.IndexOf(WeekNames, arr[0]);
                    begin = v < 1 ? 1 : (v + 1);
                }
                if (!int.TryParse(arr[1], out every))
                {
                    v = Array.IndexOf(WeekNames, arr[1]);
                    every = v < 1 ? 1 : (v + 1);
                }
                begin = begin < 1 || begin > WeekNames.Length ? 1 : begin;
                every = every < 1 || every > WeekNames.Length ? 1 : every;
                itv = every;
                if (begin == 1 && every == 1)
                {
                    return _empty;
                }
                var list = new List<int>();
                for (int i = begin; i <= WeekNames.Length; i += every)
                {
                    list.Add(GetNearDayWeek(CurrentYear, CurrentMonth, CurrentDay, i));
                }
                list.Sort();
                return list.Distinct().ToArray();
            }
            bool isHash = str.IndexOf(Hash) > -1;
            if (isHash)
            {
                arr = str.Split(Hash);
                int left = 0, right = 0;
                if (!int.TryParse(arr[0], out left))
                {
                    v = Array.IndexOf(WeekNames, arr[0]);
                    left = v < 1 ? 1 : (v + 1);
                }
                if (!int.TryParse(arr[1], out right))
                {
                    v = Array.IndexOf(WeekNames, arr[1]);
                    right = v < 1 ? 1 : (v + 1);
                }
                return new int[] { GetWeekOfMonth(CurrentYear, CurrentMonth, right, left) };
            }
            if (int.TryParse(str, out v))
            {
                return GetAllMonthWeekDay(CurrentYear, CurrentMonth, CurrentDay, v < 1 ? 1 : (v + 1));
            }
            if (str.EndsWith(L))
            {
                itv = 7;
                isLast = true;
                if (str.Length == 1)
                {
                    int totalDays = _calendar.GetDaysInMonth(CurrentYear, CurrentMonth);
                    v = GetNearWorkDayOfMonth(CurrentYear, CurrentMonth, totalDays);
                }
                else
                {
                    int.TryParse(str.Replace(L.ToString(), string.Empty), out v);
                    v = GetLastWeekOfMonth(CurrentYear, CurrentMonth, v < 1 || v > WeekNames.Length ? 1 : v);
                }
                return new int[] { v };
            }
            v = Array.IndexOf(WeekNames, str);
            return GetAllMonthWeekDay(CurrentYear, CurrentMonth, CurrentDay, v < 1 ? 1 : (v + 1));
        }

        static int[] GenerateYear(string str)
        {
            if (string.IsNullOrEmpty(str) || str.Equals(Asterisk) || str.Equals(Question))
            {
                return _empty;
            }
            string[] arr;
            bool isComma = str.IndexOf(Comma) > -1;
            if (isComma)
            {
                arr = str.Split(Comma);
                return ConvertToInt(arr, 9999, CurrentYear);
            }
            bool isHyphen = str.IndexOf(Hyphen) > -1;
            if (isHyphen)
            {
                arr = str.Split(Hyphen);
                return ConvertToIntRange(arr[0], arr[1], 1, () =>
                {
                    return _empty;
                }, 9999, CurrentYear);
            }
            if (int.TryParse(str, out var v))
            {
                return new int[] { v < CurrentYear || v > 9999 ? CurrentYear : v };
            }
            return _empty;
        }


        #endregion


        #region Convert Function

        static int[] ConvertToInt(string[] arr, int max, int min = 0)
        {
            var newArr = arr.Distinct().ToArray();
            int[] result = new int[newArr.Length];
            for (int i = 0; i < newArr.Length; i++)
            {
                result[i] = int.TryParse(newArr[i], out int v) ? (v < min || v > max ? min : v) : min;
            }
            Array.Sort(result);
            return result.Distinct().ToArray();
        }

        static int[] ConvertToInt(string[] arr, int max, int min, Func<string, int> func)
        {
            var list = new List<int>();
            for (int i = 0; i < arr.Length; i++)
            {
                if (int.TryParse(arr[i], out int v))
                {
                    list.Add(v < min || v > max ? min : v);
                    continue;
                }
                list.Add(func.Invoke(arr[i]));
            }
            list.Sort();
            return list.Distinct().ToArray();
        }

        static int[] ConvertToInt(string[] arr, int max, int min, Func<string, int[]> func)
        {
            var result = new List<int>();
            var newArr = arr.Distinct().ToArray();
            foreach (var item in newArr)
            {
                result.AddRange(func.Invoke(item));
            }
            result.Sort();
            return result.Distinct().ToArray();
        }


        static int[] ConvertToIntRange(string begin, string end, int interval, Func<int[]> defFunc, int max, int min = 0)
        {
            int bIdx = 0, eIdx = 0;
            int.TryParse(begin, out bIdx);
            bIdx = bIdx < min ? min : bIdx;
            int.TryParse(end, out eIdx);
            eIdx = eIdx > max ? max : eIdx;
            var result = new List<int>();
            if (eIdx > bIdx)
            {
                while (bIdx <= eIdx)
                {
                    result.Add(bIdx);
                    bIdx += interval;
                }
            }
            return result.Count == 0 ? defFunc.Invoke() : result.ToArray();
        }


        static int[] ConvertToIntRange(int bIdx, int eIdx, int interval, Func<int[]> defFunc)
        {
            var result = new List<int>();
            if (eIdx > bIdx)
            {
                while (bIdx <= eIdx)
                {
                    result.Add(bIdx);
                    bIdx += interval;
                }
            }
            return result.Count == 0 ? defFunc.Invoke() : result.ToArray();
        }

        #endregion


        #region Week Function


        /// <summary>
        /// 获取某月第几周的星期几的天数
        /// </summary>
        /// <param name="year">年</param>
        /// <param name="month">月</param>
        /// <param name="weekOfMonth">第几周</param>
        /// <param name="dayOfWeek">星期几</param>
        /// <returns>天数</returns>
        static int GetWeekOfMonth(int year, int month, int weekOfMonth, int dayOfWeek)
        {
            dayOfWeek = dayOfWeek < 1 || dayOfWeek > 7 ? 1 : dayOfWeek;
            DateTime firstDay = new DateTime(year, month, 1);
            int totalDays = _calendar.GetDaysInMonth(year, month);
            DayOfWeek toDayOfWeek = (DayOfWeek)(dayOfWeek - 1);
            int weekIndex = 1;
            while (weekIndex <= weekOfMonth)
            {
                if (weekIndex == weekOfMonth && toDayOfWeek == firstDay.DayOfWeek)
                {
                    return firstDay.Day;
                }
                if (firstDay.DayOfWeek == toDayOfWeek)
                {
                    firstDay = firstDay.AddDays(7);
                    weekIndex++;
                    continue;
                }
                if (firstDay.DayOfWeek != DayOfWeek.Saturday)
                {
                    firstDay = firstDay.AddDays(1);
                    continue;
                }
            }
            return firstDay.Day;
        }

        /// <summary>
        /// 获取某月最后一周的星期几的天数
        /// </summary>
        /// <param name="year">年</param>
        /// <param name="month">月</param>
        /// <param name="dayOfWeek">星期几</param>
        /// <returns>天数</returns>
        static int GetLastWeekOfMonth(int year, int month, int dayOfWeek)
        {
            dayOfWeek = dayOfWeek < 1 || dayOfWeek >= 7 ? 0 : dayOfWeek;
            DayOfWeek targetWeek = (DayOfWeek)dayOfWeek;
            DateTime firstDay = new DateTime(year, month, 1);
            int totalDays = _calendar.GetDaysInMonth(year, month);
            DateTime endDay = firstDay.AddDays(totalDays - 1);
            var endDayWeek = _calendar.GetDayOfWeek(endDay);
            int day = targetWeek - endDayWeek;
            day = day > 0 ? 7 - day : 0 - day;
            DateTime result = endDay.AddDays(0 - day);
            return result.Day;
            //for (int i = 1; i < arrDays.Length; i++)
            //{
            //    firstDay = firstDay.AddMonths(1);
            //    totalDays = _calendar.GetDaysInMonth(firstDay.Year, firstDay.Month);
            //    endDay = firstDay.AddDays(totalDays - 1);
            //    endDayWeek = _calendar.GetDayOfWeek(endDay);
            //    day = targetWeek - endDayWeek;
            //    day = day > 0 ? 7 - day : 0 - day;
            //    result = endDay.AddDays(0 - day);
            //    arrDays[i] = result.Day;
            //}
            //return arrDays;
        }

        /// <summary>
        /// 获取某月最后一周的工作日
        /// </summary>
        /// <param name="year">年</param>
        /// <param name="month">月</param>
        /// <returns>天数</returns>
        static int GetLastWorkDayOfMonth(int year, int month)
        {
            DateTime firstDay = new DateTime(year, month, 1);
            int totalDays = _calendar.GetDaysInMonth(year, month);
            DateTime endDay = firstDay.AddDays(totalDays - 1);
            var endDayWeek = endDay.DayOfWeek;
            if (endDayWeek == DayOfWeek.Sunday || endDayWeek == DayOfWeek.Saturday)
            {
                int day = DayOfWeek.Friday - endDayWeek;
                day = day > 0 ? 7 - day : 0 - day;
                return endDay.AddDays(0 - day).Day;
            }
            else
            {
                return endDay.Day;
            }
        }

        /// <summary>
        /// 获取某月某日最近的工作日
        /// </summary>
        /// <param name="year">年</param>
        /// <param name="month">月</param>
        /// <param name="endDayNum">最近的号数</param>
        /// <returns>天数</returns>
        static int GetNearWorkDayOfMonth(int year, int month, int endDayNum)
        {
            DateTime firstDay = new DateTime(year, month, 1);
            int totalDays = _calendar.GetDaysInMonth(year, month);
            DateTime lastDay = new DateTime(year, month, totalDays);
            endDayNum = endDayNum < 1 || endDayNum > totalDays ? 1 : endDayNum;
            DateTime endDay = firstDay.AddDays(endDayNum - 1);
            var endDayWeek = _calendar.GetDayOfWeek(endDay);
            int addDays = endDayWeek == DayOfWeek.Sunday ? 1 : endDayWeek == DayOfWeek.Saturday ? -1 : 0;
            var dtTo = endDay.AddDays(addDays);
            dtTo = dtTo < firstDay ? dtTo.AddDays(7) : dtTo > lastDay ? dtTo.AddDays(-7) : dtTo;
            return dtTo.Day;
        }

        /// <summary>
        /// 获取某月某日最近的星期
        /// </summary>
        /// <param name="year">年</param>
        /// <param name="month">月</param>
        /// <param name="day">日</param>
        /// <param name="weekDay">星期</param>
        /// <returns>天数</returns>
        static int GetNearDayWeek(int year, int month, int day, int weekDay)
        {
            DateTime firstDay = new DateTime(year, month, day);
            int totalDays = _calendar.GetDaysInMonth(year, month);
            DateTime endDay = new DateTime(year, month, totalDays);
            var firstDayWeek = _calendar.GetDayOfWeek(firstDay);
            int firstDayOfWeek = 1 + (int)firstDayWeek;
            int days = firstDayOfWeek > weekDay ? 7 - (firstDayOfWeek - weekDay) : weekDay - firstDayOfWeek;
            var dtTo = firstDay.AddDays(days);
            return dtTo.Day;
        }

        /// <summary>
        /// 获取某月某日最近的星期
        /// </summary>
        /// <param name="year">年</param>
        /// <param name="month">月</param>
        /// <param name="day">日</param>
        /// <param name="weekDay">星期</param>
        /// <returns>天数</returns>
        static int[] GetAllMonthWeekDay(int year, int month, int day, int weekDay, int makeCount = 0)
        {
            var result = new List<int>();
            DateTime firstDay = new DateTime(year, month, day);
            DayOfWeek dayOfWeek = (DayOfWeek)(weekDay - 1);
            DayOfWeek dofWeek = firstDay.DayOfWeek;
            if (dayOfWeek == dofWeek)
            {
                result.Add(day);
                for (int i = 0; i < makeCount; i++)
                {
                    firstDay = firstDay.AddDays(7);
                    result.Add(firstDay.Day);
                }
                return result.ToArray();
            }
            while (true)
            {
                firstDay = firstDay.AddDays(1);
                dofWeek = firstDay.DayOfWeek;
                if (dayOfWeek == dofWeek)
                {
                    result.Add(firstDay.Day);
                    for (int i = 0; i < makeCount; i++)
                    {
                        firstDay = firstDay.AddDays(7);
                        result.Add(firstDay.Day);
                    }
                    return result.ToArray();
                }
            }
        }


        #endregion


        public static bool operator ==(CronStruct lhs, CronStruct rhs)
        {
            return string.Equals(lhs._expr, rhs._expr);
        }

        public static bool operator !=(CronStruct lhs, CronStruct rhs)
        {
            return !string.Equals(lhs._expr, rhs._expr);
        }


        #region Normal Function


        public int CompareTo(object? obj)
        {
            if (obj is CronStruct cronExpr)
            {
                return this.CompareTo(cronExpr);
            }
            return obj == null ? -1 : _expr.CompareTo(obj.ToString());
        }

        public int CompareTo(CronStruct other)
        {
            return _expr.CompareTo(other._expr);
        }

        public bool Equals(CronStruct other)
        {
            return other._expr == _expr;
        }

        public override bool Equals(object? obj)
        {
            if (obj is CronStruct cronExpr)
            {
                return Equals(cronExpr);
            }
            return obj == null ? false : string.Equals(_expr, obj.ToString());
        }

        public override int GetHashCode()
        {
            return _expr.GetHashCode();
        }

        public override string ToString()
        {
            return _expr;
        }


        #endregion

    }

    internal enum CronBit : int
    {
        Second = 0,
        Minute = 1,
        Hour = 2,
        Day = 3,
        Month = 4,
        Week = 5,
        Year = 6,
    }
}
