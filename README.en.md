# CronParse

#### Description
CronParse is a simple .NET library for parsing Cron expressions

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

        
        using CronParse;
        static void Main(string[] args)
        {
            var _myCron = new CronStruct("*/30 * * * * ?");
            Console.WriteLine(_myCron.GetNextTime(DateTime.Now));
            Console.ReadLine();
        }


| Method     | Job      | Runtime  | Mean     | Error    | StdDev   | Allocated |
|----------- |--------- |--------- |---------:|---------:|---------:|----------:|
| CronStruct | .NET 6.0 | .NET 6.0 | 84.58 us | 1.662 us | 1.632 us |     520 B |
| Cronos     | .NET 6.0 | .NET 6.0 | 86.75 us | 1.292 us | 1.079 us |      88 B |
| CronStruct | .NET 7.0 | .NET 7.0 | 79.66 us | 0.910 us | 0.851 us |     520 B |
| Cronos     | .NET 7.0 | .NET 7.0 | 86.73 us | 1.638 us | 1.683 us |      88 B |
| CronStruct | .NET 8.0 | .NET 8.0 | 79.46 us | 0.814 us | 0.761 us |     520 B |
| Cronos     | .NET 8.0 | .NET 8.0 | 87.32 us | 1.645 us | 1.539 us |      88 B |


Some examples of Cron expressions

*/5 * * * * ?
0 */1 * * * ?
0 0 5-15 * * ?
0 0/3 * * * ?
0 0-5 14 * * ?
0 0/5 14 * * ?
0 0/5 14,18 * * ?
0 0/30 9-17 * * ?
0 0 10,14,16 * * ?

0 0 12 ? * WED
0 0 17 ? * TUES,THUR,SAT
0 10,44 14 ? 3 WED 
0 15 10 ? * MON-FRI
0 0 23 L * ?
0 15 10 L * ? 
0 15 10 ? * 6L
0 15 10 * * ? 2025
0 15 10 ? * 6L 2024-2025
0 15 10 ? * 6#3

"30 * * * * ?"
"30 10 * * * ?"
"30 10 1 * * ?"
"30 10 1 20 * ?"
"30 10 1 20 10 ? *"
"30 10 1 20 10 ? 2024"
"30 10 1 ? 10 * 2024"
"30 10 1 ? 10 SUN 2024"
"15,30,45 * * * * ?"
"15-45 * * * * ?"
"15/5 * * * * ?"
"15-30/5 * * * * ?"
"0 0/3 * * * ?"
"0 15 10 ? * MON-FRI"
"0 15 10 L * ?"
"0 15 10 LW * ?"
"0 15 10 ? * 5L"
"0 15 10 ? * 5#3"

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
