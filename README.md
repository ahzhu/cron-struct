# CronParse

#### 介绍
CronParse 是一个简单的 .NET 库， 用于解析 Cron 表达式;
CronParse is a simple .NET library for parsing Cron expressions

#### 软件架构
软件架构说明


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

		using CronParse;
        static void Main(string[] args)
        {
            var _myCron = new CronStruct("*/30 * * * * ?");
            Console.WriteLine(_myCron.GetNextTime(DateTime.Now));
            Console.ReadLine();
        }


| Method     | Job      | Runtime  | Mean     | Error    | StdDev   | Allocated |
|----------- |--------- |--------- |---------:|---------:|---------:|----------:|
| CronStruct | .NET 6.0 | .NET 6.0 | 83.07 us | 1.685 us | 1.316 us |     312 B |
| Cronos     | .NET 6.0 | .NET 6.0 | 84.23 us | 0.740 us | 0.618 us |      80 B |
| CronStruct | .NET 7.0 | .NET 7.0 | 79.42 us | 1.567 us | 1.866 us |     313 B |
| Cronos     | .NET 7.0 | .NET 7.0 | 85.26 us | 1.685 us | 4.196 us |      80 B |
| CronStruct | .NET 8.0 | .NET 8.0 | 79.56 us | 1.406 us | 2.016 us |     312 B |
| Cronos     | .NET 8.0 | .NET 8.0 | 84.58 us | 0.882 us | 0.782 us |      80 B |


| Method     | Job      | Runtime  | Mean     | Error    | StdDev   | Median   | Allocated |
|----------- |--------- |--------- |---------:|---------:|---------:|---------:|----------:|
| CronStruct | .NET 6.0 | .NET 6.0 | 80.86 us | 1.615 us | 4.555 us | 81.62 us |     312 B |
| Cronos     | .NET 6.0 | .NET 6.0 | 80.99 us | 1.089 us | 0.909 us | 80.88 us |      80 B |
| CronStruct | .NET 7.0 | .NET 7.0 | 82.17 us | 1.202 us | 1.004 us | 82.28 us |     312 B |
| Cronos     | .NET 7.0 | .NET 7.0 | 80.32 us | 0.956 us | 0.848 us | 80.24 us |      80 B |
| CronStruct | .NET 8.0 | .NET 8.0 | 77.44 us | 1.545 us | 3.292 us | 75.58 us |     312 B |
| Cronos     | .NET 8.0 | .NET 8.0 | 81.34 us | 1.051 us | 0.932 us | 80.98 us |      80 B |

| Method     | Job      | Runtime  | Mean     | Error    | StdDev   | Allocated |
|----------- |--------- |--------- |---------:|---------:|---------:|----------:|
| CronStruct | .NET 6.0 | .NET 6.0 | 75.16 us | 1.295 us | 1.211 us |     312 B |
| Cronos     | .NET 6.0 | .NET 6.0 | 81.26 us | 0.805 us | 0.672 us |      80 B |
| CronStruct | .NET 7.0 | .NET 7.0 | 78.67 us | 1.567 us | 3.785 us |     312 B |
| Cronos     | .NET 7.0 | .NET 7.0 | 80.07 us | 1.039 us | 0.921 us |      80 B |
| CronStruct | .NET 8.0 | .NET 8.0 | 80.36 us | 0.521 us | 0.487 us |     312 B |
| Cronos     | .NET 8.0 | .NET 8.0 | 80.40 us | 0.711 us | 0.593 us |      80 B |


一些cron表达式案例

*/5 * * * * ? 每隔5秒执行一次
0 */1 * * * ? 每隔1分钟执行一次
0 0 5-15 * * ? 每天5-15点整点触发
0 0/3 * * * ? 每三分钟触发一次
0 0-5 14 * * ? 在每天下午2点到下午2:05期间的每1分钟触发 
0 0/5 14 * * ? 在每天下午2点到下午2:55期间的每5分钟触发
0 0/5 14,18 * * ? 在每天下午2点到2:55期间和下午6点到6:55期间的每5分钟触发
0 0/30 9-17 * * ? 朝九晚五工作时间内每半小时
0 0 10,14,16 * * ? 每天上午10点，下午2点，4点 

0 0 12 ? * WED 表示每个星期三中午12点
0 0 17 ? * TUES,THUR,SAT 每周二、四、六下午五点
0 10,44 14 ? 3 WED 每年三月的星期三的下午2:10和2:44触发 
0 15 10 ? * MON-FRI 周一至周五的上午10:15触发
0 0 23 L * ? 每月最后一天23点执行一次
0 15 10 L * ? 每月最后一日的上午10:15触发 
0 15 10 ? * 6L 每月的最后一个星期五上午10:15触发 
0 15 10 * * ? 2025 2025年的每天上午10:15触发 
0 15 10 ? * 6L 2024-2025 2024年至2025年的每月的最后一个星期五上午10:15触发 
0 15 10 ? * 6#3 每月的第三个星期五上午10:15触发

"30 * * * * ?" 每半分钟触发任务
"30 10 * * * ?" 每小时的10分30秒触发任务
"30 10 1 * * ?" 每天1点10分30秒触发任务
"30 10 1 20 * ?" 每月20号1点10分30秒触发任务
"30 10 1 20 10 ? *" 每年10月20号1点10分30秒触发任务
"30 10 1 20 10 ? 2024" 2011年10月20号1点10分30秒触发任务
"30 10 1 ? 10 * 2024" 2011年10月每天1点10分30秒触发任务
"30 10 1 ? 10 SUN 2024" 2011年10月每周日1点10分30秒触发任务
"15,30,45 * * * * ?" 每15秒，30秒，45秒时触发任务
"15-45 * * * * ?" 15到45秒内，每秒都触发任务
"15/5 * * * * ?" 每分钟的每15秒开始触发，每隔5秒触发一次
"15-30/5 * * * * ?" 每分钟的15秒到30秒之间开始触发，每隔5秒触发一次
"0 0/3 * * * ?" 每小时的第0分0秒开始，每三分钟触发一次
"0 15 10 ? * MON-FRI" 星期一到星期五的10点15分0秒触发任务
"0 15 10 L * ?" 每个月最后一天的10点15分0秒触发任务
"0 15 10 LW * ?" 每个月最后一个工作日的10点15分0秒触发任务
"0 15 10 ? * 5L" 每个月最后一个星期四的10点15分0秒触发任务
"0 15 10 ? * 5#3" 每个月第三周的星期四的10点15分0秒触发任务

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
